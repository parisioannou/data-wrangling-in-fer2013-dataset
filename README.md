# Data Wrangling in FER2013 Dataset

A Data Wrangling mini project. 

_Dataset_: FER2013 taken fom Kaggle (https://www.kaggle.com/deadskull7/fer2013)

Transformed the raw dataset (csv file), into a pandas dataframe. It has been cleaned to remove unwanted data, which will allow better overall accuracy when fed into a machine learning model for training. 
Data has also beed plotted in a histogram, to allow easy representation of each class sample points.
